<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://wordpress.org/support/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', getenv('WP_DB_NAME') );

/** MySQL database username */
define( 'DB_USER', getenv('WP_DB_USER') );

/** MySQL database password */
define( 'DB_PASSWORD', getenv('WP_DB_PASSWORD') );

/** MySQL hostname */
define( 'DB_HOST', getenv('WP_DB_HOST') );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         'k},>+w1SXW13PO*7E2Mcs35f0.ZLPP>v4QJ pH.E)%7O{jzMd8kM:3j0,p]9MV z' );
define( 'SECURE_AUTH_KEY',  ' UFbR)_?}5QW,SG&~^;I}`I^RGP|JLP(&/NFX.Z1!sTV:kt#,]f%O%J&;V2vzs~+' );
define( 'LOGGED_IN_KEY',    '<G4?EsLEwMJ!]L#%h-x1_f~Sdq 8&8J)PO?RR<H?5,ii0NHOt+`lF9_O4Sz5#.+_' );
define( 'NONCE_KEY',        '.8_-i@J*O8|TD@a)14eK@7>bKO^7uo5T7e+N`_Bg5~maaoEpyld(?7$zi$/-1|k2' );
define( 'AUTH_SALT',        '&wec<2c)GX}#S}GkyExQ5Un|?++UlaVMc0nS0Dpz_n`=G?B@TZ75v8&+>OO*waip' );
define( 'SECURE_AUTH_SALT', ';gL4;Cc5V%6_A`%-t>,s!,s{@fMw-,+11fR h2/;ld58bsX2);U:R]rp(.)Hkh`3' );
define( 'LOGGED_IN_SALT',   '$DLA=`9.#PJxPW@^ Lk57;Arv{#LgBMD(F?3mD?4lyn4d}E8)j}6<jZXypk9JvxP' );
define( 'NONCE_SALT',       'S])m]CyJR6})R|M^)m*2?cU>tBVY(FYqzq%`{$ne;y}bOvU5(;DLX1<a%S,%kYa(' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the documentation.
 *
 * @link https://wordpress.org/support/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', false );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', __DIR__ . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
